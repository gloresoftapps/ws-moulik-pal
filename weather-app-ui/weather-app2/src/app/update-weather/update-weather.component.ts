import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {WeatherApiService} from '../service/weather-api.service';
import {Weather} from '../model/weather';
import {DatePipe} from '@angular/common';
import {DateConverterService} from '../service/date-converter.service';

@Component({
  selector: 'app-update-weather',
  templateUrl: './update-weather.component.html',
  styleUrls: ['./update-weather.component.css']
})
export class UpdateWeatherComponent implements OnInit {

  updateForm: FormGroup;
  weather: Weather;

  //datePipeString : string|null;
  passedWeather: Weather = new Weather();

  constructor(private formBuilder: FormBuilder,
    private weatherService: WeatherApiService,
    private router: Router,
    private datePipe: DatePipe,
    private dateConverterServ: DateConverterService) {

      //this.datePipeString = datePipe.transform(Date.now(),'yyyy-MM-dd hh:mm:ss z');

      this.weather = new Weather();
      this.updateForm = this.formBuilder.group({
        cityName: ['', [Validators.required]],
        weatherDescription: ['', [Validators.required]],
        currentTemp: ['', [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
        minTemp: ['', [Validators.required]],
        maxTemp: ['', [Validators.required]],
        sunrise: ['', [Validators.required]],
        sunset: ['', [Validators.required]]
      });

  }

  // convenience getter for easy access to form fields
  get f() { return this.updateForm.controls; }

  ngOnInit(): void {
    //console.log("Inside OnInit update-weather");

    console.log("PassedWeather object:"+window.history.state);
    this.passedWeather = history.state.weather;

    console.log("Inside OnInit update-weather" + this.passedWeather.id);

    this.updateForm = this.formBuilder.group({
      cityName: ['', [Validators.required]],
      weatherDescription: ['', [Validators.required]],
      currentTemp: ['', [Validators.required, Validators.pattern('^-?[0-9]\\d*(\\.\\d{1,2})?$')]],
      minTemp: ['', [Validators.required]],
      maxTemp: ['', [Validators.required]],
      sunrise: ['', [Validators.required]],
      sunset: ['', [Validators.required]]
    });
    this.updateForm.patchValue({
      cityName: this.passedWeather.cityName,
      weatherDescription: this.passedWeather.weatherDescription,
      currentTemp: this.passedWeather.currentTemp,
      minTemp: this.passedWeather.minTemp,
      maxTemp: this.passedWeather.maxTemp,
      //sunrise: this.datePipe.transform(new Date(this.passedWeather.sunrise*1000),'yyyy/mm/dd hh:mm:ss'),
      //sunset: this.datePipe.transform(new Date(this.passedWeather.sunset*1000),'yyyy/mm/dd hh:mm:ss')
      sunrise: this.dateConverterServ.convertUnixToDate(this.passedWeather.sunrise).toISOString(),
      sunset: this.dateConverterServ.convertUnixToDate(this.passedWeather.sunset).toISOString()
    })

  }

  onUpdate() {
    let weatherModel: Weather = this.createWeather1();

    weatherModel.id = this.passedWeather.id;
    console.log('Inside onUpdate(weather):' + this.weather.id);
    console.log("cityName:"+weatherModel.cityName);
    this.weatherService.updateWeather(weatherModel).subscribe(result => this.goToHome(),
    error => {
      console.log('Error while updating');
    });
  }

  private createWeather1(): Weather {
    var weather1: Weather = new Weather();
    weather1.cityName = this.updateForm.value.cityName;
    weather1.weatherDescription = this.updateForm.value.weatherDescription;
    weather1.currentTemp = this.updateForm.value.currentTemp;
    weather1.minTemp = this.updateForm.value.minTemp;
    weather1.maxTemp = this.updateForm.value.maxTemp;
    console.log('sunrise:'+this.dateConverterServ.convertDateToUnix(this.updateForm.value.sunrise));
    weather1.sunrise = this.dateConverterServ.convertDateToUnix(this.updateForm.value.sunrise);
    console.log('sunset:'+this.dateConverterServ.convertDateToUnix(this.updateForm.value.sunset));
    weather1.sunset = this.dateConverterServ.convertDateToUnix(this.updateForm.value.sunset);
    return weather1;
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

  validateDate(control: AbstractControl): { [key: string]: any } | null {


    // let datePattern = '/(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))|(\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d([+-][0-2]\d:[0-5]\d|Z))/';
    // if (control.value && !control.value.match(datePattern)) {
    //   return { "validateDate": true };
    // }
    // return null;

    const inputDate: string = control.value;
    if(inputDate.indexOf('T')>=0) {
      return { "validateDate": true };
    }
    return null;
  }

}
