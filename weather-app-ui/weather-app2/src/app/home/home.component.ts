import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  needRefresh: number;

  constructor() { this.needRefresh = 0; }

  ngOnInit(): void {
  }

  refreshTable(x: number) {
    this.needRefresh = x;
    //console.log("needRefresh:"+x);
  }

}
