import {Component, OnInit} from '@angular/core';
import {AuthenticateService} from '../service/authenticate.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  title = 'Get the weather based on different cities';
  constructor(public loginService1: AuthenticateService) { }

  ngOnInit(): void {
  }

}
