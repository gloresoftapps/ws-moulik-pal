import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    //console.log("Inside Auth Interceptor");
    if (sessionStorage.getItem('username') && sessionStorage.getItem('token')) {
      const token = sessionStorage.getItem('token') as string;
      const username: string = sessionStorage.getItem('username') as string;
      //console.log("username:"+username);
      //const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtb3VsaWsiLCJpc1VzZXIiOnRydWUsImV4cCI6MTYzMTE3ODg5MCwiaWF0IjoxNjMxMTYwODkwfQ.qdxySRyub3gnRj0JEzw90Zx_0_R62XIhX9SfW4qKwR7QPt73E-s9Asd2VZz80ULyWVF1whpxBM7A-V0GGtu5MQ';
      //console.log("token:"+token);
      req = req.clone({
        setHeaders: {
          //'Authorization': `Bearer `+token,
          'Authorization': token,
          'username': username
        }
      });

    }

    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    }

    window.onbeforeunload = () => this.ngOnDestroy();

    //console.log("Done with AuthInterceptor");
    //console.log(req);
    return next.handle(req);
  }

  ngOnDestroy() {
    console.log("NgOnDestroy");
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('token');
  }
}
