import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

export class User1{
  constructor(
    public status:string,
     ) {}

}

export class JwtResponse{
  constructor(
    public jwttoken:string,
     ) {}

}
@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  private authenticateUrl: string;

  constructor(private httpClient: HttpClient) {
    this.authenticateUrl = 'http://localhost:8080/authenticate';
  }

  authenticate(username: String, password: String) {
    return this.httpClient.post<any>(this.authenticateUrl, { username, password }).pipe(
      map(
        userData => {
          sessionStorage.setItem('username', username as string);
          let tokenStr= 'Bearer '+userData.token;
          sessionStorage.setItem('token', tokenStr);
          return userData;
        }
      )
    );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username');
    //console.log(!(user === null))
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('token');
  }
}
