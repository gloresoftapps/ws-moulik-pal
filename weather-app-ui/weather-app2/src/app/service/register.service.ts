import {Injectable} from '@angular/core';
import {User} from '../model/user';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private registerUrl: string;

  constructor(private http: HttpClient) {
    this.registerUrl = 'http://localhost:8080/register';
  }

  public save(user: User) {
    return this.http.post<User>(this.registerUrl, user);
  }
}
