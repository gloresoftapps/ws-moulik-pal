import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Weather} from '../model/weather';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WeatherApiService {

  private weatherApiUrl: string;

  constructor(private http: HttpClient) {
    this.weatherApiUrl = 'http://localhost:8080/weather';
  }

  public get(cityName: string): Observable<Weather> {
    return this.http.get<Weather>(this.weatherApiUrl+'/'+cityName);
  }

  public findAll(): Observable<Weather[]>  {
    return this.http.get<Weather[]>(this.weatherApiUrl);
  }

  public deleteWeather(weather: Weather) {
    return this.http.delete(this.weatherApiUrl+'/'+weather.id);
  }

  public updateWeather(weather: Weather) {
    return this.http.put<Weather>(this.weatherApiUrl+'/'+weather.id, weather);
  }
}
