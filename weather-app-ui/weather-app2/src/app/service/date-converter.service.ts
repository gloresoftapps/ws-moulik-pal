import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DateConverterService {

  constructor() { }

  public convertUnixToDate(unixDate: number): Date {
    var date = new Date(unixDate * 1000);
    console.log('Converted Unix:'+unixDate+' to Date:'+date.toISOString());
    console.log('Converted Unix:'+unixDate+' to Date:'+date);
    return date;
  }

  public convertDateToUnix(date: Date): number {
    //var num: number = 0;
    var myDate = new Date(date);
    console.log("Date:"+date);
    var num = (myDate.getTime())/1000.0;
    console.log('Converted Date:'+date+' to Unix:'+num);
    return num;
  }
}
