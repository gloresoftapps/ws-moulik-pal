import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {WeatherDataComponent} from './weather-data/weather-data.component';
import {SearchComponent} from './search/search.component';
import {AuthInterceptorService} from './service/auth-interceptor.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LogoutComponent} from './logout/logout.component';
import {UpdateWeatherComponent} from './update-weather/update-weather.component';
import {HeaderComponent} from './header/header.component';
import {DatePipe} from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    WeatherDataComponent,
    SearchComponent,
    LogoutComponent,
    UpdateWeatherComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    DatePipe,
    {provide:HTTP_INTERCEPTORS, useClass:AuthInterceptorService, multi:true } ],
  bootstrap: [AppComponent]
})
export class AppModule { }
