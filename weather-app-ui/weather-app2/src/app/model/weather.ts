export class Weather {

    id!: string;
    cityName!: string;
    weatherDescription!: string;
    currentTemp!: number;

    minTemp!: number;
    maxTemp!: number;

    sunrise!: number;
    sunset!: number;
    
}
