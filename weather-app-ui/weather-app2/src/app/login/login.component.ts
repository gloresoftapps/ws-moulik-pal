import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticateService} from '../service/authenticate.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginSection: FormGroup;

  var1 = "Username";
  var2 = "Password";

  invalidLogin = false;

  get f() { return this.loginSection.controls; }

  constructor(private router: Router,
    private loginservice: AuthenticateService,
    private formBuilder: FormBuilder) {
      this.loginSection = this.formBuilder.group({
        username: [new FormControl(' '), [Validators.required, Validators.minLength(3)]],
        password: [new FormControl(' '), [Validators.required, Validators.minLength(6)]]
      });
    }

  ngOnInit(): void {
    this.loginSection = this.formBuilder.group({
      username: ["", [Validators.required, Validators.minLength(3)]],
      password: ["", [Validators.required, Validators.minLength(6)]]
    });
  }

  checkLogin() {
    console.log('checklogin')
    if(this.loginSection != null && this.loginSection.get('username') != null &&
      this.loginSection.get('password') != null ) {
    const user = this.loginSection.get('username');
    const pass = this.loginSection.get('password');
    //console.log("user in checkLogin:"+user);
    //console.log("pass in checkLogin:"+pass);
    if(user && pass) {
    this.loginservice.authenticate(user.value as string, pass.value as string).subscribe(
      data => {
        //console.log("login successful");
        this.router.navigate(['home']);
        this.invalidLogin = false;
      },
      error => {
        this.invalidLogin = true;
        console.log("login error");

      }
    );
    }

  }

  }

}
