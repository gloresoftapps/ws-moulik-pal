import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {Weather} from '../model/weather';
import {WeatherApiService} from '../service/weather-api.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-weather-data',
  templateUrl: './weather-data.component.html',
  styleUrls: ['./weather-data.component.css']
})
export class WeatherDataComponent implements OnInit, OnChanges {

  @Input()
  needRefresh: number;

  weathers: Weather[] = [];

  constructor(private service: WeatherApiService, private router: Router) {
    this.needRefresh = 0;
  }

  ngOnInit(): void {
    this.service.findAll().subscribe(
      response => {
        //console.log(response);
        this.handleSuccessfulResponse(response);
      },
      error => {
        //this.invalidLogin = true;
        console.log('Some issue while retrieving weathers');

      }
    )
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log("change detected"+this.needRefresh);
    for(const change in changes) {
      //console.log("change detected"+change);

      this.ngOnInit();

    }
  }

  handleSuccessfulResponse(response: Weather[]) {
    this.weathers = response;
  }

  deleteRow(weather: Weather): void {
    this.service.deleteWeather(weather)
    .subscribe( data => {
      this.weathers = this.weathers.filter(u => u !== weather);
    })
  }

  updateRow(weather: Weather): void {
    this.router.navigateByUrl('/update', {state: {weather}});
  }

}
