import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {WeatherApiService} from '../service/weather-api.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  //cityName!: string;
  searchForm: FormGroup;
  //cityField: FormControl;

  invalidResp: boolean;

  @Output()
  retrieved = new EventEmitter<number>();

  cityNumber: number;

  constructor(private weatherApiService: WeatherApiService, private formBuilder: FormBuilder) {
    this.invalidResp = false;
    this.cityNumber = 0;

    this.searchForm = this.formBuilder.group({
      cityField: [new FormControl(' '), [Validators.required, Validators.minLength(3), Validators.pattern('^[a-zA-Z ]*$')]]
    });
  }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      cityField: ["", [Validators.required, Validators.minLength(3), Validators.pattern('^[a-zA-Z ]*$')]]
    });
  }

  get f() { return this.searchForm.controls; }

  callWeatherService() {
    if (this.searchForm != null && this.searchForm.get('cityField') != null) {
      const city = this.searchForm.get('cityField');
      if (city) {
        console.log('city:' + city.value)
        this.weatherApiService.get(city.value as string)
          .subscribe(data => {
            alert("Weather retrieved successfully.");
            this.retrieved.emit(++this.cityNumber);

          }, error => {
            alert("Not found");
            this.invalidResp = true;
          });
      }
    }

  }

  resetResp() {
    this.invalidResp = false;
    this.searchForm = this.formBuilder.group({
      cityField: ["", [Validators.required, Validators.minLength(3), Validators.pattern('^[a-zA-Z ]*$')]]
    });
  }

}
