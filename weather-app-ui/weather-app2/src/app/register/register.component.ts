import {Component} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../model/user';
import {RegisterService} from '../service/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {

  user: User;

  invalidEntry: boolean;

  //constructor(private route: ActivatedRoute, private router: Router, private registerService: RegisterService) {
  constructor(private router: Router, private registerService: RegisterService) {
    this.user = new User();
    this.invalidEntry = false;
  }

  onSubmit() {
    console.log(this.user);
    this.registerService.save(this.user).subscribe(result => {
      this.gotoLogin();
      this.invalidEntry = false;
    },
    error => {
      console.log('Some issue while saving users');
      this.invalidEntry = true;
    });

  }

  gotoLogin() {
    this.router.navigate(['/login']);
  }

}
