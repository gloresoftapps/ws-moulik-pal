# Weather Application

This project provides the weather information based on different cities of the world such as current temperature, Sunrise/sunset time etc.

## Installation/Setup

Project is built using Java 1.8,
Spring boot 2.5.4 (Maven 3.8.1),
Angular 12 (Node: 14.17.6, NPM: npm 6.14.15)

1. Import code from the repo into your machine.
2. **Run the Java Project**. Go inside WeatherApp folder in terminal and run `mvn spring-boot:run`<sup>1</sup>
3. **Run the Angular project**. Go inside weather-app-ui/weather-app2 folder in terminal and run `ng serve -o`<sup>2</sup>
4. After both the above projects are compiled/built and running, you should see a browser window open. The application is now ready to be used.


## Application Usage
1. Register yourself as a new user by clicking on **Register** and entering details.
2. After successful registration, user can **Login** with the credentials. The Search window will open.
3. Enter any city name e.g London and the weather data for that city will be retrieved and logged under in the table.
   *User can enter only Letters as search inputs.


##Project Implementations
* It uses JWT Token to validate a user. After successful, each request call to the backend api is accompanied with a Jwt token.
* Currently uses an in-memory database to store user and weather details. So, the data is not persistent.

##In-progress
1. **Register as an admin:** Currently all users will be registered as *User*.
2. **Validations on Updating City Data** Currently validations on fields such as sunrise and sunset timestamps are not implemented.


## Notes
<sup>1</sup> Need to have Maven installed to run this command.  
<sup>2</sup> Need to have Node.js and npm installed to run the project.
## Credits
Moulik Pal (*m23pal@gmail.com*)
