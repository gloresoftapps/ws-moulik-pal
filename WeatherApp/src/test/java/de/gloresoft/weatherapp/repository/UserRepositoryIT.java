package de.gloresoft.weatherapp.repository;

import de.gloresoft.weatherapp.v1.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class UserRepositoryIT {

    @Autowired
    UserRepository userRepository;

    @Test
    void findByUsername() {
        User user = new User();
        user.setUsername("user1");
        user.setPassword("pass1");
        user.setDateOfBirth(LocalDate.of(1990,9,1));
        userRepository.save(user);

        User user1 = userRepository.findByUsername("user1");
        assertEquals("user1", user1.getUsername());
    }
}