package de.gloresoft.weatherapp.repository;

import de.gloresoft.weatherapp.v1.model.User;
import de.gloresoft.weatherapp.v1.model.WeatherDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DataJpaTest
class WeatherRepositoryIT {

    @Autowired
    WeatherRepository weatherRepository;
    @Autowired
    UserRepository userRepository;

    @Test
    void findAllFromJoin() {
        WeatherDto weatherDto = new WeatherDto();
        weatherDto.setId(1L);
        weatherDto.setCityName("London");
        User user = new User();
        user.setUsername("user1");
        user.setPassword("pass1");
        user.setDateOfBirth(LocalDate.of(1990,9,1));
        userRepository.save(user);
        weatherDto.getUsers().add(user);
        weatherRepository.save(weatherDto);
        Iterable<WeatherDto> weatherDtos = weatherRepository.findAllFromJoin("user1");
        assertEquals(StreamSupport.stream(weatherDtos.spliterator(), false)
                .count(), 1);
    }
}