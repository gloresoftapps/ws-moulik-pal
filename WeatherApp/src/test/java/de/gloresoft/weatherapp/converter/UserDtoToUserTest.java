package de.gloresoft.weatherapp.converter;

import de.gloresoft.weatherapp.v1.model.User;
import de.gloresoft.weatherapp.v1.model.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserDtoToUserTest {

    UserDtoToUser userDtoToUser;

    @BeforeEach
    void setUp() {
        userDtoToUser = new UserDtoToUser();
    }

    @Test
    void convert() {
        UserDto source = new UserDto();
        source.setUsername("user1");
        source.setPassword("pass1");
        source.setRole("ROLE_USER");
        source.setDateOfBirth("01-10-1990");
        User user = userDtoToUser.convert(source);
        assertEquals("user1",user.getUsername());
    }

    @Test
    void convertInvalidDate() {
        UserDto source = new UserDto();
        source.setUsername("user1");
        source.setPassword("pass1");
        source.setRole("ROLE_USER");
        source.setDateOfBirth("01-10-199A");

        assertThrows(DateNotParsedException.class, () -> {
                userDtoToUser.convert(source);
        });
    }
}