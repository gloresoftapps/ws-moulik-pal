package de.gloresoft.weatherapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.gloresoft.weatherapp.service.WeatherService;
import de.gloresoft.weatherapp.v1.model.WeatherDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


class WeatherControllerTest {

    WeatherController weatherController;
    @Mock
    WeatherService weatherService;

    WeatherDto weatherDto;

    MockMvc mockMvc;

    private static final String API_URL = "/city";
    private static final String cityName = "London";

    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);

        weatherController = new WeatherController(weatherService);
        mockMvc = MockMvcBuilders.standaloneSetup(weatherController)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();

        weatherDto = new WeatherDto();
        weatherDto.setCityName(cityName);
        weatherDto.setWeatherDescription("Cloudy");
    }


    @Test
    void getWeatherForName() throws Exception {

        final String username = "user1";

        when(weatherService.findByName(cityName, username)).thenReturn(weatherDto);

        mockMvc.perform(get("/weather/London").header("username", username))
                .andExpect(status().isOk());
        verify(weatherService, times(1)).findByName(cityName, username);

    }

    @Test
    void getWeatherForNameNoHeader() throws Exception {

        final String username = "user1";

        when(weatherService.findByName(cityName, username)).thenReturn(weatherDto);

        mockMvc.perform(get("/weather/London"))
                .andExpect(status().isBadRequest());
        verify(weatherService, never()).findByName(cityName, username);

    }

    @Test
    void getAllWeatherData() throws Exception {

        final String username = "user1";
        List<WeatherDto> list = new ArrayList<>();
        list.add(weatherDto);
        when(weatherService.getAllWeatherByUser(username)).thenReturn(list);

        mockMvc.perform(get("/weather").header("username", username))
                .andExpect(status().isOk());
        verify(weatherService, times(1)).getAllWeatherByUser(username);

    }

    @Test
    void getAllWeatherDataNoHeader() throws Exception {

        final String username = "user1";
        List<WeatherDto> list = new ArrayList<>();
        list.add(weatherDto);
        when(weatherService.getAllWeatherByUser(username)).thenReturn(list);

        mockMvc.perform(get("/weather"))
                .andExpect(status().isBadRequest());
        verify(weatherService, never()).getAllWeatherByUser(username);

    }

    @Test
    void updateWeatherById() throws Exception {

        final String username = "user1";
        List<WeatherDto> list = new ArrayList<>();
        list.add(weatherDto);
        doNothing().when(weatherService).updateWeatherById(weatherDto, 1L, username);
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(weatherDto);
        mockMvc.perform(put("/weather/1").header("username", username)
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isAccepted());
        verify(weatherService, times(1)).updateWeatherById(weatherDto, 1L, username);
    }

    @Test
    void updateWeatherByIdNoHeader() throws Exception {

        final String username = "user1";
        List<WeatherDto> list = new ArrayList<>();
        list.add(weatherDto);
        doNothing().when(weatherService).updateWeatherById(weatherDto, 1L, username);
        ObjectMapper om = new ObjectMapper();
        String json = om.writeValueAsString(weatherDto);
        mockMvc.perform(put("/weather/1")
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isBadRequest());
        verify(weatherService, never()).updateWeatherById(weatherDto, 1L, username);
    }

    @Test
    void deleteWeatherById() throws Exception {

        doNothing().when(weatherService).deleteWeatherById(1L);

        mockMvc.perform(delete("/weather/1"))
                .andExpect(status().isNoContent());

        verify(weatherService, times(1)).deleteWeatherById(1L);

    }
}