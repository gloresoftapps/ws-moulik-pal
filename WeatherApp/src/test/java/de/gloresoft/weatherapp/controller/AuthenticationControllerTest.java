package de.gloresoft.weatherapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.gloresoft.weatherapp.config.JwtUtil;
import de.gloresoft.weatherapp.converter.UserDtoToUser;
import de.gloresoft.weatherapp.service.CustomUserDetailsService;
import de.gloresoft.weatherapp.v1.model.AuthenticationRequest;
import de.gloresoft.weatherapp.v1.model.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AuthenticationControllerTest {

    AuthenticationController controller;

    @Mock AuthenticationManager authenticationManager;
    @Mock CustomUserDetailsService userDetailsService;
    @Mock JwtUtil jwtUtil;
    @Mock UserDtoToUser userDtoToUser;

    @Mock UserDetails userDetails;
    @Mock Authentication authentication;

    MockMvc mockMvc;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);

        controller = new AuthenticationController(authenticationManager, userDetailsService, jwtUtil, userDtoToUser);
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new RestResponseEntityExceptionHandler())
                .build();

    }

    @Test
    void createAuthenticationToken() throws Exception {

        AuthenticationRequest authRequest = new AuthenticationRequest();
        String authReqJson = new ObjectMapper().writeValueAsString(authRequest);
        when(authenticationManager.authenticate(authentication))
                .thenReturn(authentication);
        when(userDetailsService.loadUserByUsername(anyString())).thenReturn(userDetails);
        when(jwtUtil.generateToken(userDetails)).thenReturn("Token");

        mockMvc.perform(post("/authenticate")
                .contentType(MediaType.APPLICATION_JSON).content(authReqJson))
                .andExpect(status().isOk());
    }

    @Test
    void saveUser() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        UserDto userDto = new UserDto();
        userDto.setUsername("user1");
        userDto.setPassword("pass1");
        userDto.setRole("ROLE_USER");
        String userDtoJson = mapper.writeValueAsString(userDto);
        when(userDetailsService.save(any())).thenReturn(any());

        mockMvc.perform(post("/register")
                        .contentType(MediaType.APPLICATION_JSON).content(userDtoJson))
                .andExpect(status().isOk());

        verify(userDetailsService, times(1)).save(any());
    }
}