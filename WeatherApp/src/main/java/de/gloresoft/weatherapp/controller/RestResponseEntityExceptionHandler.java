package de.gloresoft.weatherapp.controller;

import de.gloresoft.weatherapp.converter.DateNotParsedException;
import de.gloresoft.weatherapp.service.ResourceNotFoundException;
import de.gloresoft.weatherapp.service.UserPresentAlreadyException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<Object> handleNotFoundException(Exception e) {
        return new ResponseEntity<Object>("Resource Not Found", new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Object> usernameNotFoundException(Exception e) {
        return new ResponseEntity<Object>("Username Not Found"+e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UserPresentAlreadyException.class)
    public ResponseEntity<Object> usernameAlreadyPresentException(Exception e) {
        return new ResponseEntity<Object>("Username Possibly Present Already:"+e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DateNotParsedException.class)
    public ResponseEntity<Object> dateNotParsedException(Exception e) {
        return new ResponseEntity<Object>(e.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }
}
