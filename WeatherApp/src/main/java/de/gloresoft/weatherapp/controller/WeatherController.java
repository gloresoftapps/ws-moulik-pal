package de.gloresoft.weatherapp.controller;

import de.gloresoft.weatherapp.service.WeatherService;
import de.gloresoft.weatherapp.v1.model.WeatherDto;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/weather")
public class WeatherController {

    private final WeatherService weatherService;

    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("/{cityName}")
    @ResponseStatus(HttpStatus.OK)
    public WeatherDto getWeatherForName(@PathVariable("cityName") String name, HttpServletRequest req) {
        String username = req.getHeader("username");
        if(username==null) {
            throw new IllegalArgumentException("Username not found");
        }
        System.out.println("username:"+username);
        return weatherService.findByName(name, username);
    }

    /*@GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<WeatherDto> getAllWeatherData() {
        return weatherService.getAllWeather();
    }*/

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<WeatherDto> getAllWeatherData(HttpServletRequest req) {
        String username = req.getHeader("username");
        if(username==null) {
            throw new IllegalArgumentException("Username not found");
        }
        System.out.println("username:"+username);
        return weatherService.getAllWeatherByUser(username);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void updateWeatherById(@PathVariable("id") String id, @RequestBody WeatherDto weatherDto, HttpServletRequest req) {
        String username = req.getHeader("username");
        if(username==null) {
            throw new IllegalArgumentException("Username not found");
        }
        System.out.println("username:"+username);
        weatherService.updateWeatherById(weatherDto, Long.valueOf(id), username);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteWeatherById(@PathVariable("id") Long id) {
        weatherService.deleteWeatherById(id);
    }
}
