package de.gloresoft.weatherapp.v1.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name="weather")
public class WeatherDto {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String cityName;

    private String weatherDescription;

    private Double currentTemp;

    private Double minTemp;
    private Double maxTemp;

    private Long sunrise;
    private Long sunset;

    @ManyToMany
    @JoinTable(name="weather_user", joinColumns = @JoinColumn(name="weather_id"), inverseJoinColumns = @JoinColumn(name="user_id"))
    private Set<User> users = new HashSet<>();

    public WeatherDto() {
    }

    public WeatherDto(String cityName, String weatherDescription, Double currentTemp, Double minTemp, Double maxTemp, Long sunrise, Long sunset) {
        this.cityName = cityName;
        this.weatherDescription = weatherDescription;
        this.currentTemp = currentTemp;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    public WeatherDto(Long id, String cityName, String weatherDescription, Double currentTemp, Double minTemp, Double maxTemp, Long sunrise, Long sunset) {
        this.id = id;
        this.cityName = cityName;
        this.weatherDescription = weatherDescription;
        this.currentTemp = currentTemp;
        this.minTemp = minTemp;
        this.maxTemp = maxTemp;
        this.sunrise = sunrise;
        this.sunset = sunset;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public Double getCurrentTemp() {
        return currentTemp;
    }

    public void setCurrentTemp(Double currentTemp) {
        this.currentTemp = currentTemp;
    }

    public Double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
    }

    public Double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Long getSunrise() {
        return sunrise;
    }

    public void setSunrise(Long sunrise) {
        this.sunrise = sunrise;
    }

    public Long getSunset() {
        return sunset;
    }

    public void setSunset(Long sunset) {
        this.sunset = sunset;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WeatherDto)) return false;
        WeatherDto weatherDto = (WeatherDto) o;
        return Objects.equals(cityName, weatherDto.cityName) && Objects.equals(weatherDescription, weatherDto.weatherDescription) && Objects.equals(currentTemp, weatherDto.currentTemp) && Objects.equals(minTemp, weatherDto.minTemp) && Objects.equals(maxTemp, weatherDto.maxTemp) && Objects.equals(sunrise, weatherDto.sunrise) && Objects.equals(sunset, weatherDto.sunset);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName, weatherDescription, currentTemp, minTemp, maxTemp, sunrise, sunset);
    }
}
