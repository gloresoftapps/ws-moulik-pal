package de.gloresoft.weatherapp.repository;

import de.gloresoft.weatherapp.v1.model.WeatherDto;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


public interface WeatherRepository extends CrudRepository<WeatherDto, Long> {

    @Query(value = "SELECT W.ID, W.CITY_NAME , W.WEATHER_DESCRIPTION, W.CURRENT_TEMP, W.MAX_TEMP, W.MIN_TEMP, W.SUNRISE, W.SUNSET, " +
            " FROM WEATHER W INNER JOIN WEATHER_USER C ON C.WEATHER_ID = W.ID INNER JOIN USER U ON U.ID = C.USER_ID WHERE U.USERNAME=(:username)",
        nativeQuery = true)
    Iterable<WeatherDto> findAllFromJoin(String username);


}
