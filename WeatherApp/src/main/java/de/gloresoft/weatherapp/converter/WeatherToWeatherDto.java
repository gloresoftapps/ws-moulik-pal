package de.gloresoft.weatherapp.converter;

import de.gloresoft.weatherapp.domain.CityWeather;
import de.gloresoft.weatherapp.v1.model.WeatherDto;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
public class WeatherToWeatherDto implements Converter<CityWeather, WeatherDto> {

    @Nullable
    @Override
    public WeatherDto convert(CityWeather source) {
        if (source == null) {
            return null;
        }
        System.out.println(source.getMain().getTempMax());
        System.out.println(source.getMain().getTempMin());
        final WeatherDto weatherDto = new WeatherDto();
        weatherDto.setCityName(source.getName());
        weatherDto.setWeatherDescription(source.getWeather().get(0).getDescription());

        weatherDto.setCurrentTemp(source.getMain().getTemp());
        if(source.getMain().getTempMax() == null) {
            weatherDto.setMaxTemp(source.getMain().getTemp());
        } else {
            weatherDto.setMaxTemp(source.getMain().getTempMax());
        }
        if(source.getMain().getTempMax() == null) {
            weatherDto.setMinTemp(source.getMain().getTemp());
        } else {
            weatherDto.setMinTemp(source.getMain().getTempMin());
        }

        System.out.println("Sunrise:"+source.getSys().getSunrise());
        weatherDto.setSunrise(source.getSys().getSunrise());
        System.out.println("Sunset:"+source.getSys().getSunset());
        weatherDto.setSunset(source.getSys().getSunset());
        return weatherDto;
    }
}
