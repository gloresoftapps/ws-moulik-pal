package de.gloresoft.weatherapp.service;

import de.gloresoft.weatherapp.converter.WeatherToWeatherDto;
import de.gloresoft.weatherapp.domain.CityWeather;
import de.gloresoft.weatherapp.repository.UserRepository;
import de.gloresoft.weatherapp.repository.WeatherRepository;
import de.gloresoft.weatherapp.v1.model.User;
import de.gloresoft.weatherapp.v1.model.WeatherDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class WeatherServiceImpl implements WeatherService {

    private final RestTemplate restTemplate;
    private final String url;
    private final String appId;
    private final WeatherRepository weatherRepository;
    private final WeatherToWeatherDto weatherToWeatherDto;
    private final UserRepository userRepository;

    public WeatherServiceImpl(RestTemplate restTemplate, @Value("${api.url}") String url,
                              @Value("${appid}") String appId, WeatherRepository weatherRepository,
                              WeatherToWeatherDto weatherToWeatherDto, UserRepository userRepository) {
        this.restTemplate = restTemplate;
        this.url = url;
        this.appId = appId;
        this.weatherRepository = weatherRepository;
        this.weatherToWeatherDto = weatherToWeatherDto;
        this.userRepository = userRepository;
    }

    @Override
    public WeatherDto findByName(String name, String username) throws ResourceNotFoundException {

        String baseUrl = url;
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(baseUrl);
        builder.queryParam("q", "{name}");
        builder.queryParam("appid", appId);
        builder.queryParam("units", "metric");

        DefaultUriBuilderFactory factory = new DefaultUriBuilderFactory(baseUrl);
        factory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);
        restTemplate.setUriTemplateHandler(factory);
        String uri= builder.buildAndExpand(name).encode().toUriString();

        try {
            CityWeather cityWeather = restTemplate.getForObject(uri, CityWeather.class);
            WeatherDto weatherDto = weatherToWeatherDto.convert(cityWeather);
            System.out.println("username:" + username);
            User user = userRepository.findByUsername(username);
            weatherDto.getUsers().add(user);

            List<WeatherDto> list = StreamSupport
                    .stream(weatherRepository.findAll().spliterator(), false)
                    .filter(wDto -> wDto.getCityName().equalsIgnoreCase(name))
                    .collect(Collectors.toList());

            if(!list.isEmpty()) {
            //if(id.length==1) {
                weatherDto.setId(list.get(0).getId());
            }
            weatherRepository.save(weatherDto);
            return weatherDto;
        } catch (HttpClientErrorException.NotFound exception) {
            throw new ResourceNotFoundException(exception);
        }
    }

    /*//@Override
    public String getByName(String name) {
        List<String> weatherList = new ArrayList<>();
        weatherRepository.findAll().forEach(weather -> {
            if(weather.getCityName().equalsIgnoreCase(name)) {
                weatherList.add(name);
            }
        });
        return weatherList.get(0);
    }*/

    @Override
    public void updateWeatherById(WeatherDto weatherDto, Long id, String username) {
        weatherDto.setId(id);
        User user = userRepository.findByUsername(username);
        weatherDto.getUsers().add(user);
        weatherRepository.save(weatherDto);
    }

    @Override
    public void deleteWeatherById(Long id) {
        weatherRepository.deleteById(id);
    }

    @Override
    public void deleteWeatherByMultipleIds(List<Long> ids) {
        ids.stream().forEach(id -> weatherRepository.deleteById(id));
    }

    @Override
    public List<WeatherDto> getAllWeather() {
        List<WeatherDto> weatherDtoList = new ArrayList<>();
        weatherRepository.findAll()
                .iterator()
                .forEachRemaining(weatherDto -> weatherDtoList.add(weatherDto));
        return weatherDtoList;
    }

    @Override
    public List<WeatherDto> getAllWeatherByUser(String username) {

        return StreamSupport.stream(weatherRepository.findAllFromJoin(username).spliterator(), false)
                .collect(Collectors.toList());

    }
}
