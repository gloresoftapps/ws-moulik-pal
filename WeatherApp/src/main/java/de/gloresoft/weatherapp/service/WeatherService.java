package de.gloresoft.weatherapp.service;

import de.gloresoft.weatherapp.v1.model.WeatherDto;

import java.util.List;

public interface WeatherService {

    WeatherDto findByName(String name, String username);

    //boolean existsByName(String name);

    void updateWeatherById(WeatherDto weatherDto, Long id, String username);

    void deleteWeatherById(Long id);

    void deleteWeatherByMultipleIds(List<Long> ids);

    List<WeatherDto> getAllWeather();

    List<WeatherDto> getAllWeatherByUser(String username);
}
