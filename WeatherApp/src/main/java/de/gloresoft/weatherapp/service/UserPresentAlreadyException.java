package de.gloresoft.weatherapp.service;

public class UserPresentAlreadyException extends RuntimeException {

    public UserPresentAlreadyException() {
    }

    public UserPresentAlreadyException(String message) {
        super(message);
    }

    public UserPresentAlreadyException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserPresentAlreadyException(Throwable cause) {
        super(cause);
    }
}
